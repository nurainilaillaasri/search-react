import logo from '../logo.svg';
import '../App.css';
import Language from '../components/LanguageComponent';
import About from './About';
import {
  useParams,
} from "react-router-dom";

function Javascript() {
  let { id } = useParams();
  // const languageList = [
  //   {
  //     name: 'HTML & CSS',
  //     id: 'HTML & CSS',
  //     image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
  //   },
  //   {
  //     name: 'JavaScript',
  //     id: 'JavaScript',
  //     image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
  //   },
  //   {
  //     name: 'React',
  //     id: 'React',
  //     image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
  //   },
  //   {
  //     name: 'Ruby',
  //     id: 'Ruby',
  //     image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
  //   },
  //   {
  //     name: 'Ruby on Rails',
  //     id: 'Ruby on Rails',
  //     image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
  //   },
  //   {
  //     name: 'Python',
  //     id: 'Python',
  //     image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
  //   }
  // ];

  return (
    <div className="App">
      <header className="App-header">
        <h3>ID: {id}</h3>
        {/* {languageList.map((languageItem) => { */}
          {/* if (languageItem.name === id) {
            return (
              <Language
                name={languageItem.name}
                image={languageItem.image}
              />
            )
          } */}

        {/* })} */}

          <About 
            id={id}
            name={'lai'}
          />
      </header>
    </div>
  );
}

export default Javascript;
