import logo from '../logo.svg';
import React, { Component } from 'react';
import '../App.css';
import Language from '../components/LanguageComponent';

class About extends Component {

  languageList = {
    list: [
      {
        name: 'HTML & CSS',
        id: 'HTML & CSS',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
      },
      {
        name: 'JavaScript',
        id: 'JavaScript',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
      },
      {
        name: 'React',
        id: 'React',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
      },
      {
        name: 'Ruby',
        id: 'Ruby',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
      },
      {
        name: 'Ruby on Rails',
        id: 'Ruby on Rails',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
      },
      {
        name: 'Python',
        id: 'Python',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
      }
    ],

  };

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      list: this.languageList.list,
      isSeach: true,
    }
  }

  listLanguage = (language) => {
    return (
      language.map((languageItem) => {
        if (this.props.id === languageItem.name) {
          return (
            <Language
              name={languageItem.name}
              image={languageItem.image}
              isBtn={false}
            />
          )
        } else if (this.props.id === undefined) {
          return (
            <Language
              name={languageItem.name}
              image={languageItem.image}
              isBtn={true}
            />
          )
        }
      })
    )
  }

  search = (language) => {
    const data = []
    for (let i = 0; i < this.languageList.list.length; i++) {
      if (this.languageList.list[i].name.toLowerCase().indexOf(language.target.value.toLowerCase()) > -1) {
        data.push(this.languageList.list[i])
      } else if (language.target.value.toLowerCase() === '') {
        data.push(this.languageList.list[i])
      }
    }
    return this.setState({
      list: data
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Iki Halaman About
          </p>
          <div>
            {this.state.isSeach ? (
              <input type="text" id="search" onChange={this.search} />
            ) : (<></>)}
          </div>
          {this.listLanguage(this.state.list)}
        </header>
      </div>
    );
  }
}

export default About;
